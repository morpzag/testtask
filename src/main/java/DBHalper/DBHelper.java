package DBHalper;

import Handler.User;

import java.sql.*;
import java.util.HashMap;

public class DBHelper {
    private static DBHelper instance;

    private DBHelper() {
    }

    public static DBHelper getInstance() {
        if (instance == null) {
            instance = new DBHelper();
        }
        return instance;
    }

    public HashMap<String, User> putAllUsersToUserMap() {
        HashMap<String, User> userMap = new HashMap<>();
        String sql = "SELECT * FROM users";
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                User user = new User();
                user.setName(rs.getString("name"));
                user.setLastName(rs.getString("lastname"));
                userMap.put(user.getName(), user);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userMap;
    }

    public User getUserByName(String name) {
        User user = new User();
        String sql = "SELECT * FROM users WHERE name like ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)
        ) {
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();
            user.setName(rs.getString("name"));
            user.setLastName(rs.getString("lastname"));

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return user;
    }


    public void updateUserLastName(User user) {
        String sql = "UPDATE users SET lastname = ? WHERE name = ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, user.getLastName());
            pstmt.setString(2, user.getName());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private Connection connect() {
        String url = "jdbc:sqlite:./src/main/resources/TestDB.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public int countUsers() {
        int numberOfUsers = 0;
        String sql = "SELECT COUNT(*) AS total FROM users";
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            numberOfUsers = rs.getInt("total");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return numberOfUsers;
    }
}
