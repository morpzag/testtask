package Handler;

import com.google.gson.JsonElement;

class StandardResponse {
    private StatusResponse status;
    private String message;
    private JsonElement data;


    StandardResponse(StatusResponse status, String message) {
        this.status = status;
        this.message = message;
    }

    StandardResponse(StatusResponse status, JsonElement data) {
        this.status = status;
        this.data = data;
    }
}
