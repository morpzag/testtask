package Handler;

import DBHalper.DBHelper;

import java.util.HashMap;


public class UserServiceMapImpl implements UserService {

    private void setUserMap(HashMap<String, User> userMap) {
        this.userMap = userMap;
    }

    public UserServiceMapImpl() {
        updateUserMap();
    }

    public int getUserMapSize() {
        return userMap.size();
    }

    private HashMap<String, User> userMap;

    @Override
    public User getUser(String name) {
        return userMap.get(name);
    }

    @Override
    public User editUserLastName(User forEdit) throws UserException {
        try {
            if (forEdit.getName() == null)
                throw new UserException("Field \"name\" can't be empty!");
            User toEdit = userMap.get(forEdit.getName());

            if (toEdit == null)
                throw new UserException("User not found!");

            if (forEdit.getLastName() != null) {
                DBHelper.getInstance().updateUserLastName(forEdit);
                updateUserMap();
            }
            toEdit = userMap.get(forEdit.getName());
            return toEdit;
        } catch (Exception ex) {
            throw new UserException(ex.getMessage());
        }
    }

    @Override
    public boolean isUserExist(String name) {
        return userMap.containsKey(name);
    }

    private void updateUserMap() {
        setUserMap(DBHelper.getInstance().putAllUsersToUserMap());
    }
}
