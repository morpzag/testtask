package Handler;

public interface UserService {
    User getUser(String name);

    User editUserLastName(User user) throws UserException;

    boolean isUserExist(String name);
}
