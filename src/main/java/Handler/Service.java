package Handler;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static spark.Spark.get;
import static spark.Spark.put;

public class Service {
    private static Service instance;

    private Service() {
        startService();
    }

    public static Service getInstance() {
        if (instance == null) {
            instance = new Service();
        }
        return instance;
    }

    private void startService() {
        final UserService userService = new UserServiceMapImpl();
        get("/users/:name", (request, response) -> {
            response.type("application/json");
            if (userService.isUserExist(request.params(":name"))) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(userService.getUser(request.params(":name")))));
            } else
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR,
                        new Gson().toJsonTree("User not found")));
        });

        put("/users/:name", (request, response) -> {
            response.type("application/json");

            JsonElement element = new JsonParser().parse(request.body());
            JsonObject object = element.getAsJsonObject();
            String lastName = object.getAsJsonPrimitive("lastName").getAsString();
            User toEdit = new User();
            toEdit.setLastName(lastName);
            toEdit.setName(request.params(":name"));
            User editedUser = userService.editUserLastName(toEdit);

            if (editedUser != null) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(editedUser)));
            } else {
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR,
                        new Gson().toJson("User not found or error in edit")));
            }
        });
    }
}
