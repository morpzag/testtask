import DBHalper.DBHelper;
import Handler.User;
import Handler.UserServiceMapImpl;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class DBHelperTest {
    @Test
    public void testGetUser() {
        DBHelper dbHelper = DBHelper.getInstance();
        String expected = "Аркадий";
        User source = dbHelper.getUserByName("Аркадий");
        String actual = source.getName();
        assertEquals("Query has returned wrong user or null!",expected,actual);
    }

    @Test
    public void testUpdateUserLastName() {
        DBHelper dbHelper = DBHelper.getInstance();
        String expected = "SucTest";
        User source = dbHelper.getUserByName("Аркадий");

        String tempLastName = source.getLastName();
        source.setLastName(expected);
        dbHelper.updateUserLastName(source);
        String actual = dbHelper.getUserByName("Аркадий").getLastName();
        assertEquals("Last names are not equals!",expected,actual);

        expected = tempLastName;
        source.setLastName(expected);
        dbHelper.updateUserLastName(source);
        actual = dbHelper.getUserByName("Аркадий").getLastName();
        assertEquals("Last names are not equals!",expected,actual);
    }

    @Test
    public void testPutAllUsersToHashMap() {
        DBHelper dbHelper = DBHelper.getInstance();
        UserServiceMapImpl userServiceMap = new UserServiceMapImpl();
        int expected = dbHelper.countUsers();
        int actual = userServiceMap.getUserMapSize();
        assertEquals("Number of records in DB and userMap size are not equals!",expected,actual);
    }

}
