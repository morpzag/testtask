import Handler.User;
import Handler.UserException;
import org.junit.Test;
import Handler.UserServiceMapImpl;

import static junit.framework.TestCase.assertEquals;

public class UseServiceMapImplTest {
    @Test
    public void testGetUser() {
        String source = "Аркадий";
        String expected = "Аркадий";
        User actual = new UserServiceMapImpl().getUser(source);
        assertEquals("Names are not equals!", expected, actual.getName());
    }

    @Test
    public void testEditUser() throws UserException {
        UserServiceMapImpl userServiceMap = new UserServiceMapImpl();
        String expected = "TestSuccess";
        User source = userServiceMap.getUser("Аркадий");
        String tempLastName = source.getLastName();

        source.setLastName(expected);
        userServiceMap.editUserLastName(source);
        String actual = userServiceMap.getUser(source.getName()).getLastName();
        assertEquals("Last name of user was not updated or edited!", expected, actual);

        expected = tempLastName;
        source.setLastName(expected);
        userServiceMap.editUserLastName(source);
        actual = userServiceMap.getUser(source.getName()).getLastName();
        assertEquals("Last name of user was not updated or edited!", expected, actual);
    }
}
