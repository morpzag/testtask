import Handler.Service;
import Handler.User;
import com.google.gson.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.apache.http.entity.*;

import java.io.IOException;
import static junit.framework.TestCase.assertEquals;

public class TestService {

    @Test
    public void testGet() throws IOException {
        Service.getInstance();
        String expected = "Аркадий";
        User source = getUserByName(expected);
        String actual = source.getName();
        assertEquals("Names are not equals, or request error!",expected,actual);
    }

    @Test
    public void testPut() throws IOException {
        Service.getInstance();
        String testUser = "Аркадий";
        String expected = "TestUser";
        String url = "http://localhost:4567/users/" + testUser;
        HttpClient client = HttpClientBuilder.create().build();
        User tempUser = getUserByName(testUser);
        String tempLastName = tempUser.getLastName();

        HttpPut put = new HttpPut(url);
        put.setEntity(getEntity(expected));
        client.execute(put);
        User source = getUserByName(testUser);
        String actual = source.getLastName();
        assertEquals("Last name was not changed or request error!",expected,actual);

        expected = tempLastName;
        put.setEntity(getEntity(expected));
        client.execute(put);
        source = getUserByName(testUser);
        actual = source.getLastName();
        assertEquals("Last name was not changed or request error! (2)",expected,actual);

    }

    private User getUserByName(String name) throws IOException {
        User user;
        String url = "http://localhost:4567/users/" + name;
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(new HttpGet(url));
        HttpEntity entity = response.getEntity();
        String responseString = EntityUtils.toString(entity);
        JsonElement element = new JsonParser().parse(responseString);
        JsonObject object = element.getAsJsonObject();
        JsonObject userJson = object.getAsJsonObject("data");
        user = new Gson().fromJson(userJson, User.class);
        return user;
    }

    private StringEntity getEntity(String lastName) {
        return new StringEntity("{" +
                "\"lastName\":\""+ lastName +"\"" +
                "}",
                ContentType.APPLICATION_JSON);
    }
}
